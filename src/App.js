import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";

import Login from "./components/Login";
import Signup from "./components/Signup";
import Home from "./components/Home";
import Token from "./components/Token";
import Subreddit from "./components/Subreddit";

class App extends React.Component {
  render() {
    let { getToken, saveToken } = Token();
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" render={() => {
              return <>
                <Home token={getToken()} />
              </>
            }}/>
            <Route
              path="/login"
              render={() => <Login saveToken={saveToken} />}
            />
            <Route path="/signup" component={Signup} />
            
            <Route path="/r/:sub_reddit" component = {Subreddit}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
