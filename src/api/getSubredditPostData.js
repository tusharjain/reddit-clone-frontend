import axios from "axios";

function getSubredditPostData(saveData, sort, name) {
    console.log("Getting new data");
    axios.get(`https://api-mountblue-react-clone.herokuapp.com/getposts?subreddit=${name}`)
        .then((res) => {console.log("axios");saveData(res.data.posts)})
        .catch((err) => console.log(err));
}

export { getSubredditPostData };