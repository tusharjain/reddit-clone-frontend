import axios from "axios";

function getPostData(saveData, sort, country) {
    console.log("Getting new data");
    axios.get(`https://api-mountblue-react-clone.herokuapp.com/getposts?sort=${sort}&country=${country}`)
        .then((res) => {console.log("axios");saveData(res.data.posts)})
        .catch((err) => console.log(err));
}

export { getPostData };