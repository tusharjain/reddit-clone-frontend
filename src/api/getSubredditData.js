import axios from "axios";

function getSubredditData(saveData, name) {
    
    console.log("Getting new data", saveData.toString(), name);
    axios.get(`https://api-mountblue-react-clone.herokuapp.com/getsubreddits/${name}`)
        .then((res) => saveData(res.data))
        .catch((err) => console.log(err));
}

export { getSubredditData };