import React from "react";

import redditIcon from "../assets/reddit.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowUp, faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { faCommentAlt } from "@fortawesome/free-regular-svg-icons";
import TimeAgo from "javascript-time-ago";
import en from "javascript-time-ago/locale/en";
TimeAgo.addDefaultLocale(en);
const timeAgo = new TimeAgo("en-US");

const arrowUp = <FontAwesomeIcon icon={faArrowUp} />;
const arrowDown = <FontAwesomeIcon icon={faArrowDown} />;
const commentIcon = <FontAwesomeIcon icon={faCommentAlt} />;

function getPostBodyData(data) {
  if (data.type === "post") {
    return data.body;
  } else if (data.type === "media") {
    if (data.type.image) {
      return (
        <img
          src={data.type.image}
          alt="user_post"
          style={{ width: 320, height: 240 }}
        />
      );
    }
    if (data.type.video) {
      return (
        <video width="320" height="240" controls>
          <source src={data.type.video} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      );
    }
  }
}

function getPostTimeDate(createdAt) {
  let currentDate = Math.floor(Date.now());
  createdAt = Math.floor(Date.parse(createdAt));
  console.log(currentDate, createdAt);
  return timeAgo.format(currentDate - createdAt, "round");
}

function getDP(sub_reddit) {
  if (sub_reddit) {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        class="_30bZQzX8IR92H3gw3vlaLF"
        style={{ width: "1rem" }}
      >
        <path d="M16.5,2.924,11.264,15.551H9.91L15.461,2.139h.074a9.721,9.721,0,1,0,.967.785ZM8.475,8.435a1.635,1.635,0,0,0-.233.868v4.2H6.629V6.2H8.174v.93h.041a2.927,2.927,0,0,1,1.008-.745,3.384,3.384,0,0,1,1.453-.294,3.244,3.244,0,0,1,.7.068,1.931,1.931,0,0,1,.458.151l-.656,1.558a2.174,2.174,0,0,0-1.067-.246,2.159,2.159,0,0,0-.981.215A1.59,1.59,0,0,0,8.475,8.435Z"></path>
      </svg>
    );
  } else {
    <img src={redditIcon} alt="default dp" />;
  }
}

function getHost(sub_reddit, user_name) {
  if (sub_reddit) {
    <a href="./">
      <span className="text-start fw-bold">
        {getDP(sub_reddit)}&nbsp; r/{sub_reddit}
      </span>{" "}
    </a>;
  } else {
    <a href="./">
      <span className="text-start fw-bold">
        {getDP(sub_reddit)}&nbsp; u/{user_name}
      </span>{" "}
    </a>;
  }
}

export default function Post(props) {
  return (
    <div className="row post border border-secondary rounded-3 flex-column flex-md-row">
      <div className="col-1 vote-section text-muted fs-5 rounded-start">
        <div className="arrowUp">{arrowUp}</div>
        <div className="voteCount">
          {props.data.up_votes - props.data.down_votes}
        </div>
        <div className="arrowDown">{arrowDown}</div>
      </div>

      <div className="col-11 post-section">
        <div className="row post-metadata text-muted">
          <div className="col">
            {getHost(props.data.sub_reddit, props.data.user_name)}
            
            &nbsp;&#183;&nbsp; posted by &nbsp;
            <a href="./">
              <span className="text-start">
                u/
                {props.data.user_name}
              </span>{" "}
            </a>
            &nbsp;&#183;&nbsp;
            <span className="text-start">
              {getPostTimeDate(props.data.createdAt)}
            </span>
          </div>
        </div>
        <div className="row post-title">
          <div className="col  text-start fs-4">{props.data.title}</div>
        </div>
        <div className="row post-body">
          <div className="col text-start"> {getPostBodyData(props.data)}</div>
        </div>
        <div className="row post-action text-muted">
          <div className="col">
            <a href="/" className="btn text-muted mx-3 fw-bold">
              {commentIcon}&nbsp;{props.data.comments_count}&nbsp; Comment
            </a>
            <a href="/" className="btn text-muted mx-2 fw-bold">
              Share
            </a>
            <a href="/" className="btn text-muted mx-2 fw-bold">
              Hide
            </a>
            <a href="/" className="btn text-muted mx-2 fw-bold">
              Save
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
