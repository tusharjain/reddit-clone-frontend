/**
 * 
 * {
    "no_of_pages": 2,
    "posts_per_page": 5,
    "page": 0,
    "no_of_posts": 9,
    "posts": [
        {
            "id": 5,
            "title": "mgggon",
            "body": "im a good boy",
            "type": "post",
            "is_NSFW": false,
            // "image": null,
            "video": null,
            "poll": null,
            "country": "IN",
            "sub_reddit": null,
            "user_name": "mon",
            "hot": 0,
            "top": 0,
            "share_count": 0,
            "up_votes": 0,
            "down_votes": 0,
            "comments_count": 0,
            "createdAt": "2021-07-22T14:05:29.000Z",
            "updatedAt": "2021-07-22T14:05:29.000Z"
        },
 */
import React from "react";

import Post from "./Post";
import SubredditList from "./SubredditList";
import Filter from "./Filter";

import { getPostData } from "./getPostData";

export default class Main extends React.Component {
  constructor() {
    super();
    this.state = {
      postData: [],
      sort: "new",
      country: "in",
    };
    this.savePostData = this.savePostData.bind(this);
  }

  setFilter = (sort, country) => {
    this.setState({
      sort: sort,
      country: country,
    });
  };

  savePostData(postData) {
    console.log("savingposts", this.state);
    this.setState({ postData: postData });
    console.log("savedposts", this.state);
  }

  componentDidMount() {
    console.log("didMount", this.state);
    getPostData(this.savePostData, this.state.sort, this.state.country);
    // this.setState({sort:"new", country:"in"});
    console.log("didMount", this.state);
  }

  shouldComponentUpdate(x, newState) {
    console.log("shouldUpdate", this.state, newState);
    if (
      newState.postData.length !== this.state.postData.length ||
      newState.sort !== this.state.sort ||
      newState.country !== this.state.country
    ) {
      getPostData(this.savePostData, this.state.sort, this.state.country);
      return true;
    }
    return false;
  }

  render() {
    console.log("render", this.state);
    const postCollection = this.state.postData.map((post) => (
      <Post data={post} key={post.id} />
    ));
    return (
      <>
        <Filter getFilter={this.setFilter} currentFilter={this.state} />
        <div className="container">
          <div className="row main">
            <div className="col-md-9 p-5">{postCollection}</div>
            <div className="col-md-3">
              <SubredditList />
            </div>
          </div>
        </div>
      </>
    );
  }
}
