/**
 * {
    "url_name": "bootcamp101",
    "title": "The is the subreddit for a bootcamp",
    "about": "In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available",
    "type": "public",
    "sub_reddit_image": "subreddit.png",
    "rules": "[]",
    "is_NSFW": false,
    "admin_name": null,
    "createdAt": "2021-07-23T10:04:01.000Z",
    "updatedAt": "2021-07-23T10:04:01.000Z"
}
 * 
 * 
 */

import React from "react";

import Post from "./Post";
import SubredditList from "./SubredditList";
import Filter from "./Filter";
import SubredditHeader from "./SubredditHeader";

import { getSubredditPostData } from "../api/getSubredditPostData";
import { getSubredditData } from "../api/getSubredditData";
import { faTruckMonster } from "@fortawesome/free-solid-svg-icons";

export default class SubredditMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      postData: [],
      sort: "new",
      subreddit_name: props.subreddit_name,
      subreddit_title: "",
      subreddit_about: "",
    };
    this.saveSubredditPostData = this.saveSubredditPostData.bind(this);
    this.saveSubredditData = this.saveSubredditData.bind(this);
  }

  setFilter = (sort) => {
    this.setState({
      sort: sort,
    });
  };

  saveSubredditPostData(postData) {
    this.setState({ postData: postData });
  }

  saveSubredditData(data) {
    this.setState({
      subreddit_title: data.title,
      subreddit_about: data.about,
    });
    console.log("saveSubredditData", this.state); 
  }

  componentDidMount() {
    console.log("didMount", this.state);
    getSubredditData(this.saveSubredditData, this.state.subreddit_name);
    // getPostData(this.saveSubredditPostData, this.state.sort);
    getSubredditPostData(this.saveSubredditPostData, this.state.sort, )
  }

  shouldComponentUpdate(x, newState) {
    console.log("shouldComponentUpdate", this.state, newState);
    // if (
    //   newState.postData.length !== this.state.postData.length ||
    //   newState.sort !== this.state.sort
    // ) {
    //   getPostData(this.saveSubredditPostData, this.state.sort);
    //   return true;
    // }
    // return false;

    return true;
  }

  render() {
    console.log("render", this.state );
    const postCollection = this.state.postData.map((post) => (
      <Post data={post} key={post.id} />
    ));
    return (
      <>
        <SubredditHeader data={this.state}/>
        <Filter getFilter={this.setFilter} currentFilter={this.state} />
        <div className="container">
          <div className="row main">
            <div className="col-md-9 p-5">{postCollection}</div>
            <div className="col-md-3">
              <SubredditList />
            </div>
          </div>
        </div>
      </>
    );
  }
}
