import React from "react";

import Navbar from "./Navbar";

import Main from "./Main";

export default function Home(props) {
  return (
    <div className="home">
      <Navbar token={props.token} />
      <Main/>
    </div>
  );
}
