import React from "react";
import Axios from "axios";

const SignupStatus = {
    INITIAL: 0,
    SUCCESS: 1,
    ERROR: -1,
    IN_PROGRESS: 2,
}

export default class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      username: "",
      password: "",
      registrationStep: 0,
      signupStatus: SignupStatus.INITIAL,
      errorResponse: [""],
      isNewEmail: false,
    };
  }

  readEmail = async (event) => {
    await this.setState(() => {
      return { 
        email: event.target.value 
      };
    });
  };

  readUsername = (event) => {
    this.setState({ 
      username: event.target.value 
    });
  };

  readPassword = (event) => {
    this.setState({ 
      password: event.target.value 
    });
  };

  doSignup = (event) => {
    event.preventDefault();
    Axios.post("https://api-mountblue-react-clone.herokuapp.com/signup", {
      user_name: this.state.username,
      email: this.state.email,
      password: this.state.password,
    })
      .then((response) => {
        this.setState({ 
          username: "", 
          password: "", 
          signupStatus: SignupStatus.SUCCESS,
        });
      })
      .catch((err) => {
        console.error(err, JSON.parse(err.request.response).error);

        this.setState({
          signupStatus: SignupStatus.ERROR,
          errorResponse: JSON.parse(err.request.response).error,
        }, () => {
          console.log(this.state.errorResponse);
        });
      });
  };

  checkEmailValid = (event) => {
    event.preventDefault();

    Axios.post("https://api-mountblue-react-clone.herokuapp.com/signup", {
      email: this.state.email,
    })
      .catch((res) => {
        let emailparam = JSON.parse(res.request.response).error.email; 
        if(res.request.response !== undefined) {
          
          if(
            res.request.response.error !== undefined && 
            res.request.response.error.email !== undefined
          ) {
            

            if (emailparam) {
              this.setState({
                errorResponse: [emailparam],
                signupStatus: SignupStatus.ERROR,
              });
            } else {
              this.setState({ 
                registrationStep: 1, 
                signupStatus: SignupStatus.INITIAL
            });
            }
          }
        } else {
            this.setState({
                errorResponse: [emailparam],
                signupStatus: SignupStatus.ERROR,
            });
        }
      });
  };

  emailReenter = (event) => {
    event.preventDefault();
    this.setState({ registrationStep: 0 });
  };

  displayAlertBox = () => {
    if (this.state.signupStatus === SignupStatus.ERROR) {
      return (
        <div className="alert alert-danger" role="alert">
          {Object.values(this.state.errorResponse).length > 1 ? 
                Object.values(this.state.errorResponse).map((err) => (
                    <p key={err}>{err}</p>
                ))
                : 
                Object.values(this.state.errorResponse)[0]
            }
        </div>
      );
    } 
    
    else if (this.state.signupStatus === SignupStatus.SUCCESS) {
      setTimeout(() => {
        this.props.history.push("/login");
      }, 1000);
      
      return (
        <div className="alert alert-success" role="alert">
          Signed up Successfully
        </div>
      );
    }
    
    else {
      return <div></div>;
    }
  };

  render() {
    if (this.state.registrationStep) {
      return (
        <div className="container-fluid signup">
          <div className="row">
            <div className="col signup-header">
              <p>
                <span>Choose your username</span>
                <br />
                Your username is how other community members will see you. This
                name will be used to credit you for things you share on Reddit.
                What should we call you?
              </p>
            </div>
            <hr />
            <div className="col form-section">
              {this.displayAlertBox()}
              <form className="signin form-signin" onSubmit={this.doSignup}>
                <div className="form-floating mb-3">
                  <input
                    className="username form-control"
                    id="floatingInput"
                    type="text"
                    onChange={this.readUsername}
                    placeholder="Username"
                  ></input>
                  <label
                    htmlFor="floatingInput"
                    className="text-uppercase text-muted"
                  >
                    choose a username
                  </label>
                </div>

                <div className="form-floating">
                  <input
                    className="password form-control"
                    id="floatingPassword"
                    type="password"
                    onChange={this.readPassword}
                    placeholder="Password"
                  ></input>
                  <label
                    htmlFor="floatingPassword"
                    className="text-uppercase text-muted"
                  >
                    password
                  </label>
                </div>
                <br />
                <button
                  type="submit"
                  className="button btn btn-primary w-100 text-uppercase"
                >
                  Sign up
                </button>
              </form>
            </div>
            <hr />
            <div className="col footer-section text-start">
              <div>
                <a
                  href="/signup"
                  className="link-primary"
                  onClick={this.emailReenter}
                >
                  Go Back
                </a>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="login-container">
          <div className="image-section">
            <img
              src="https://www.redditstatic.com/accountmanager/bbb584033aa89e39bad69436c504c9bd.png"
              alt="side pane abstract design"
              className="entry-page-image"
            />
          </div>
          <div className="form-section">
            <div className="form-title">
              <p>
                <span>Sign up</span>
                <br />
                By continuing, you agree to our User Agreement and Privacy
                Policy.{" "}
              </p>
            </div>
            <div>
              {this.displayAlertBox()}
              <form
                className="signin form-signin"
                onSubmit={this.checkEmailValid}
              >
                <div className="form-floating mb-3">
                  <input
                    className="username form-control"
                    id="floatingInput"
                    type="text"
                    onChange={this.readEmail}
                    placeholder="EMAIL"
                    value={this.state.email}
                  ></input>
                  <label
                    htmlFor="floatingInput"
                    className="text-uppercase text-muted"
                  >
                    email
                  </label>
                </div>

                <br />
                <button
                  type="submit"
                  className="button btn btn-primary w-100 text-uppercase"
                  onClick={this.checkEmailValid}
                >
                  continue
                </button>
              </form>
              <div className="form-footer">
                <p>
                  Already a redditor?{" "}
                  <a href="/login" className="footer-sign-up ">
                    Log In
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}
