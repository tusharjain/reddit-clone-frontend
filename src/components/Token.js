export default function Token() {
  function getToken() {
    return (localStorage.getItem("token"));
  }

  function saveToken(tokenStr) {
    localStorage.setItem("token", tokenStr);
  }

  return { getToken, saveToken };
}
