import React from "react";

import dp from "../assets/subredditdp.jpg";
import bannerImg from "../assets/bannerImg.jpg";

export default function SubredditHeader(props) {
  console.log("SubredditHeader", props.data);
  return (
    <div className="subredditHeader-container">
      <div className="subredditHeader-data">
        <div className="subredditHeader-body">
          <div className="subredditHeader-dp">
            <img src={dp} alt="dp" />
          </div>
          <div className="info">
            <div className="text">
              <div className="subredditHeader-title "><h2>{props.data.subreddit_title}</h2></div>
              <div className="subredditHeader-name ">r/{props.data.subreddit_name}</div>
            </div>
            <div className="subredditHeader-join">Join</div>
          </div>
        </div>
      </div>
    </div>
  );
}
