import React from "react";

import Navbar from "./Navbar";

import SubredditMain from "./SubredditMain";

export default function Subreddit(props) {
  console.log("subreddit route props", props, props.match.params.sub_reddit)
  return (
    <div className="home">
      <Navbar token={props.token} />
      <SubredditMain subreddit_name={props.match.params.sub_reddit}/>
    </div>
  );
}
