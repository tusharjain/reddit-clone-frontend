import React from "react";

export default class Filter extends React.Component {
  selectSort = (event) => {
    console.log(
      event,
      event.target,
      event.target.classList,
      event.target.classList[0],
      event.target.parentElement.childNodes
    );
    for (let child of event.target.parentElement.childNodes) {
      if (child.classList.contains("sort-select")) {
        child.classList.remove("sort-select");
      }
    }

    event.target.classList.add("sort-select");
    if (event.target.classList[0] !== this.props.currentFilter.sort) {
      this.props.getFilter(
        event.target.classList[0],
        this.props.currentFilter.country
      );
    }
  };

  selectCountry = (event) => {
    console.log(event.target.value);
    if (event.target.value !== this.props.currentFilter.country) {
      this.props.getFilter(this.props.currentFilter.sort, event.target.value);
    }
  };

  render() {
    return (
      <div className="container my-3 rounded-3">
        <div className="row">
          <div className="col-md-9">
            <div className="row filter-container rounded-3">
              <div className="col-6">
                <div className="sort-pills d-flex">
                  <div
                    className="new btn m-2 sort-select"
                    onClick={this.selectSort}
                  >
                    New
                  </div>
                  <div className="hot btn m-2" onClick={this.selectSort}>
                    Hot
                  </div>
                  <div className="top btn m-2" onClick={this.selectSort}>
                    Top
                  </div>
                </div>
              </div>
              <div className="col-6 my-2">
                <div className="filter-country w-50">
                  <select
                    className="form-select"
                    aria-label="Default select example"
                    onClick={this.selectCountry}
                    defaultValue="in"
                  >
                    <option value="in">
                      India
                    </option>
                    <option value="us">USA</option>
                    <option value="rs">Russia</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
