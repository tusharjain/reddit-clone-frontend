import React from "react";

import logo from "../assets/icon.png";

export default class Navbar extends React.Component {
  getUserOptions = (token) => {
    if(token) {
      return (
        <div className="dropdown">
              <button
                className="btn btn-secondary dropdown-toggle"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                User Profile
              </button>
              <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li>
                  <a className="dropdown-item" href="./">
                    Action
                  </a>
                </li>
                <li>
                  <a className="dropdown-item" href="./">
                    Another action
                  </a>
                </li>
                <li>
                  <a className="dropdown-item" href="./">
                    Something else here
                  </a>
                </li>
              </ul>
            </div>
      )
    }
    else {
      <ul className="navbar-nav ">
              <li className="nav-item active">
                <a
                  className="nav-link btn btn-outline-secondary m-2"
                  href="/login"
                >
                  Log In
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link btn btn-secondary text-light m-2"
                  href="/signup"
                >
                  Sign up
                </a>
              </li>
            </ul>
    }
  }


  selectNavbar(token) { 
    
      return (
        <div className="navbar-container">
          <nav className="navbar justify-content-between navbar-expand-sm navbar-light bg-light  p-2">
            <a className="navbar-brand" href="/">
              <img src={logo} alt="logo" style={{ width: "50px" }} />
              TrendIt
            </a>

            <form className="form-inline my-2 my-lg-0 ">
              <input
                className="form-control mr-sm-2"
                type="search"
                placeholder="Search"
              />
            </form>

            {this.getUserOptions(token)}
          </nav>
        </div>
      );
    
  }

  render() {
    return this.selectNavbar(this.props.token);
  }
}
