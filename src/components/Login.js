import React from "react";
import Axios from "axios";
import { withRouter } from "react-router-dom";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      loginStatus: 0,
    };
  }

  readUsername = (event) => {
    this.setState({ username: event.target.value });
  };

  readPassword = (event) => {
    this.setState({ password: event.target.value });
  };

  doLogin = (event) => {
    event.preventDefault();
    Axios.post("https://api-mountblue-react-clone.herokuapp.com/login", {
      user_name: this.state.username,
      password: this.state.password,
    })
      .then((res) => {
        this.setState({ username: "", password: "", loginStatus: 1 });
        this.props.saveToken(res.data.token);
        setTimeout(() => {
          this.props.history.push("/");
        }, 2000);
      })
      .catch((res) => {
        this.setState({ loginStatus: -1 });
      });
  };

  displayAlertBox = () => {
    if (this.state.loginStatus === -1) {
      return (
        <div className="alert alert-danger" role="alert">
          Enter proper credentials
        </div>
      );
    } else if (this.state.loginStatus === 1) {
      return (
        <div className="alert alert-success" role="alert">
          Login Success
        </div>
      );
    } else {
      return <div></div>;
    }
  };

  render() {
    return (
      <div className="login-container">
        <div className="image-section">
          <img
            src="https://www.redditstatic.com/accountmanager/bbb584033aa89e39bad69436c504c9bd.png"
            alt="side pane abstract design"
            className="entry-page-image"
          />
        </div>
        <div className="form-section">
          <div className="form-title">
            <p>
              <span>Login</span>
              <br />
              By continuing, you agree to our User Agreement and Privacy Policy.{" "}
            </p>
          </div>
          <div>
            {this.displayAlertBox()}
            <form className="signin form-signin" onSubmit={this.doLogin}>
              <div className="form-floating mb-3">
                <input
                  className="username form-control"
                  id="floatingInput"
                  type="text"
                  onChange={this.readUsername}
                  placeholder="Username"
                  value={this.state.username}
                ></input>
                <label
                  htmlFor="floatingInput"
                  className="text-uppercase text-muted"
                >
                  Username
                </label>
              </div>

              <div className="form-floating">
                <input
                  className="password form-control"
                  id="floatingPassword"
                  type="password"
                  onChange={this.readPassword}
                  placeholder="Password"
                  value={this.state.password}
                ></input>
                <label
                  htmlFor="floatingPassword"
                  className="text-uppercase text-muted"
                >
                  Password
                </label>
              </div>
              <br />
              <button
                type="submit"
                className="button btn btn-primary w-100 text-uppercase"
              >
                Sign in
              </button>
            </form>
            <div className="form-footer">
              <p>
                New to Reddit?{" "}
                <a href="/signup" className="footer-sign-up">
                  Sign up
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
